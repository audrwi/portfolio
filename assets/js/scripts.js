$(function () {

	$('header nav span').click(function () {
		$(this).parent().toggleClass('active');
	});

	$('#career div.trigger').mousemove(function (e) {
		var x = e.clientX, y = e.clientY;
		if ($(window).width() >= 800)
			$('#career div.trigger').siblings('div').css({
				top:	(y + 20) + 'px',
				left:	x - ($(this).siblings('div').width() / 2) + 'px'
			});
	});

	$("a[href^='#']").click(function (e) {
		e.preventDefault();
		var top = $($(this).attr('href')).offset().top;
		$('html, body').animate({ scrollTop: top }, top);
		$('header nav').removeClass('active');
	});

	$('form[name=contact]').submit(function (e) {
		e.preventDefault();
		var values = {};
		$(this).children('input, textarea').each(function () { values[this.name] = this.value; });
		$.ajax({
			url:		'_sendMessage',
			method:		'POST',
			data:		values
		}).done(function (res) {
			$('form[name=contact]').addClass(res ? 'error' : 'success').removeClass(res ? 'success' : 'error');
			$('form[name=contact] div.error_messages').empty();
			if (res)
				$.parseJSON(res).forEach(function (value) {
					$('form[name=contact] div.error_messages').append('<p>' + value + '</p>');
				});
			else
				$('form[name=contact]').children('input, textarea').not('[type=submit]').val('');
		});
	});

	document.addEventListener('contextmenu', function (e) { e.preventDefault() });

});
