<?php

function ucname ($string) {
	$string = ucwords(strtolower($string));
	foreach (array('-', '\'') as $delimiter)
		if (strpos($string, $delimiter) !== false)
			$string = implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
	return $string;
}

function getEmailingContent ($emailing, $data) {
	$url = 'http://' . $_SERVER[HTTP_HOST] . '/www/emailings/' . $emailing . '.php?';
	foreach ($data as $key => $value)
		$url .= '&' . $key . '=' . urlencode($value);
	return file_get_contents($url);
}

$errors	= array ();
$regex	= array (
	'first_name'	=> "/^([a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ](-| |')?){2,20}[a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ]$/i",
	'last_name'		=> "/^([a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ](-| |')?){2,20}[a-zàáâãäåçèéêëìíîïðòóôõöøùúûüýÿ]$/i",
	'email'			=> '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-z\-0-9]+\.)+[a-z]{2,}))$/i',
	'message'		=> '/^[\S\s]{1,300}$/i'
);

foreach ($regex as $key => $value)
	if (!isset($_POST[$key]))
		$errors[] = ucfirst(str_replace('_', ' ', $key)) . ' not sent.';
	else if (trim($_POST[$key]) === '')
		$errors[] = ucfirst(str_replace('_', ' ', $key)) . ' not specified.';
	else if (!preg_match($value, trim($_POST[$key])))
		$errors[] = 'Invalid ' . str_replace('_', ' ', $key) . '.';

if (empty($errors) && !$errors[0]) {
	$emailings = array (
		'confirmation'	=> array (
			'receiver'		=> strtolower(trim($_POST['email'])),
			'sender'		=> 'audreyboucher95@gmail.com',
			'sender_name'	=> 'Audrey Boucher',
			'subject'		=> 'Thanks for your message on audreyboucher.fr'
		),
		'notification'	=> array (
			'receiver'		=> 'audreyboucher95@gmail.com',
			'sender'		=> strtolower(trim($_POST['email'])),
			'sender_name'	=> ucname(trim($_POST['first_name'])) . ' ' . ucname(trim($_POST['last_name'])),
			'subject'		=> 'New message on audreyboucher.fr'
		)
	);
	foreach ($emailings as $key => $value) {
		$header = 'From: "' . $value['sender_name'] . '" <' . $value['sender'] . '> via audreyboucher.fr' . "\r\n";
		$header .= 'Reply-To: <' . $value['sender'] . '>' . "\r\n";
		$header .= 'MIME-Version: 1.0' . "\r\n";
		$header .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$content = getEmailingContent($key, array (
			'first_name'	=> ucname(trim($_POST['first_name'])),
			'last_name'		=> ucname(trim($_POST['last_name'])),
			'email'			=> strtolower(trim($_POST['email'])),
			'message'		=> trim($_POST['message'])
		));
		mail($value['receiver'], $value['subject'], $content, $header);
	}
} else
	echo json_encode($errors);

?>
