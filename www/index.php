<!DOCTYPE html>
<html lang="en">
<head>
	<script type="text/javascript" src="//try.abtasty.com/d44a056c5b912dac8ecbc07ab7d71745.js"></script>
	<meta charset="utf-8">
	<title>Audrey Boucher - Portfolio</title>
	<meta name="author" content="Audrey Boucher">
	<meta name="copyright" content="&copy; Copyright 2017 | Audrey Boucher">
	<meta name="description" content="">
	<meta name="keywords" lang="en" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="robots" content="index, follow">
	<meta property="og:title" content="Portfolio - Audrey Boucher">
	<meta property="og:url" content="http://www.audreyboucher.fr/">
	<meta property="og:image" content="imgs/logo.svg">
	<meta property="og:description" content="">
	<meta property="og:type" content="website">
	<link rel="canonical" href="http://www.audreyboucher.fr/">
	<meta name="twitter:site" content="@AudreyBoucher">
	<meta name="twitter:creator" content="@AudreyBoucher">
	<link href="assets/css/styles.css" rel="stylesheet">
	<link rel="icon" href="assets/imgs/favicon/favico.ico">
	<link rel="apple-touch-icon" sizes="152x152" href="assets/imgs/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="assets/imgs/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="assets/imgs/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="assets/imgs/favicon/manifest.json">
	<link rel="mask-icon" href="assets/imgs/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
	<meta name="application-name" content="Portfolio - Audrey Boucher">
	<meta name="google-site-verification" content="_FCGO9xFmJ3EbjiOkxw6MEQVs1kFU6TBGH6KAUTAJdM">
</head>
<body class="index">
	<header>
		<nav>
			<span class="icon-menu"></span>
			<ul>
				<a href="#career"><li>My career</li></a>
				<a href="#skills"><li>My skills</li></a>
				<a href="#message"><li>Send me a message</li></a>
				<a href="#contact"><li>How to contact me</li></a>
			</ul>
		</nav>
		<article>
			<h1 class="icon-logo">Audrey Boucher</h1>
			<h2>Armed with a pencil and my trusty computer Alejandro, I create internet specific to your image.</h2>
			<a href="assets/imgs/cv_audrey_boucher.pdf" class="icon-download" download>Download my CV</a>
		</article>
	</header>
	<section id="career">
		<h3>My career<span class="icon-clover"></span></h3>
		<div>
			<article class="formation">
				<div class="trigger"></div>
				<span class="num">01</span>
				<figure>
					<img src="assets/imgs/logos/iesa.svg" alt="IESA Multimédia - 2012-2015">
				</figure>
				<span class="year">2012</span>
				<div>
					<h4>IESA Multimédia</h4>
					<h5>From 2012 to 2015</h5>
					<p>Title « Head of multimedia project »</p>
					<p>Learning graphism, video editing, animation, web development and communication basics</p>
				</div>
			</article>
			<article class="experience">
				<div class="trigger"></div>
				<span class="num">02</span>
				<figure>
					<img src="assets/imgs/logos/netrock.svg" alt="NetRock - 2013">
				</figure>
				<span class="year">2013</span>
				<div>
					<h4>NetRock</h4>
					<h5>2013</h5>
					<p>Graphic design internship</p>
					<p>Logo design, business card, setting print materials and web page, video editing</p>
				</div>
			</article>
			<article class="experience">
				<div class="trigger"></div>
				<span class="num">03</span>
				<figure>
					<img src="assets/imgs/logos/leparisien.svg" alt="Le Parisien - 2013-2015">
				</figure>
				<span class="year">2013</span>
				<div>
					<h4>Le Parisien</h4>
					<h5>From 2013 to 2015</h5>
					<p>Alternating contract</p>
					<p>Integration mailings, landing pages, adding / updating / removing content, management responsive (mobile)</p>
					<p>Learning Symfony2 and teamwork</p>
				</div>
			</article>
			<article class="formation">
				<div class="trigger"></div>
				<span class="num">04</span>
				<figure>
					<img src="assets/imgs/logos/42.svg" alt="42 School - 2015-2018">
				</figure>
				<span class="year">2015</span>
				<div>
					<h4>42 School</h4>
					<h5>From 2015 to 2018</h5>
					<p>Digital Technology Certification</p>
					<p>Learning programmation and development</p>
				</div>
			</article>
		</div>
		<div>
			<article class="experience">
				<div class="trigger"></div>
				<span class="num">05</span>
				<figure>
					<img src="assets/imgs/logos/yewtech.svg" alt="Yew Tech - 2017">
				</figure>
				<span class="year">2017</span>
				<div>
					<h4>Yew Tech</h4>
					<h5>2017</h5>
					<p>Fullstack JS development internship</p>
					<p>Thinking about the best way to valorize the product, creating all design, structure and code of an innovating project from nothing</p>
					<p>Learning NodeJS, AngularJS, MongoDB and Arduino</p>
				</div>
			</article>
			<article class="experience">
				<div class="trigger"></div>
				<span class="num">06</span>
				<figure>
					<img src="assets/imgs/logos/salesmachine.svg" alt="Salesmachine - 2017">
				</figure>
				<span class="year">2017</span>
				<div>
					<h4>Salesmachine</h4>
					<h5>2017</h5>
					<p>Freelance contract</p>
					<p>Total remake of the website with a new design and an improvement of the code optimization</p>
				</div>
			</article>
			<article class="experience">
				<div class="trigger"></div>
				<span class="num">07</span>
				<figure>
					<img src="assets/imgs/logos/abtasty.svg" alt="AB Tasty - 2017-?">
				</figure>
				<span class="year">2017</span>
				<div>
					<h4>AB Tasty</h4>
					<h5>2017 to nowadays</h5>
					<p>Technical Expert</p>
					<p>Creating tests for several clients with Javascript and CSS, always searching the best way to implement solutions for their issues</p>
					<p>Learning how to train teams and clients</p>
				</div>
			</article>
		</div>
	</section>
	<section id="skills">
		<h3>My skills<span class="icon-clover"></span></h3>
		<article>
			<h4>Languages</h4>
			<?php
				$not = array ('.', '..');
				function getName($filename) {
					return ucwords(str_replace('_', ' ', preg_replace('/^([0-9]{2}-)([a-z0-9_]+)(?:.png|.svg)$/', '$2', $filename)));
				}
				foreach (scandir('../assets/imgs/logos/languages', 0) as $language) {
					if (!in_array($language, $not) && ($name = getName($language))) {
			?>
			<figure>
				<img src="assets/imgs/logos/languages/<?php echo $language; ?>" alt="<?php echo $name; ?>">
				<figcaption><?php echo $name; ?></figcaption>
			</figure>
			<?php } } ?>
		</article>
		<article>
			<h4>Frameworks / libraries</h4>
			<?php
				foreach (scandir('../assets/imgs/logos/frameworks', 0) as $framework) {
					if (!in_array($framework, $not) && ($name = getName($framework))) {
			?>
			<figure>
				<img src="assets/imgs/logos/frameworks/<?php echo $framework; ?>" alt="<?php echo $name; ?>">
				<figcaption><?php echo $name; ?></figcaption>
			</figure>
			<?php } } ?>
			<?php
				foreach (scandir('../assets/imgs/logos/libraries', 0) as $library) {
					if (!in_array($library, $not) && ($name = getName($library))) {
			?>
			<figure>
				<img src="assets/imgs/logos/libraries/<?php echo $library; ?>" alt="<?php echo $name; ?>">
				<figcaption><?php echo $name; ?></figcaption>
			</figure>
			<?php } } ?>
		</article>
		<article>
			<h4>Applications</h4>
			<?php
				foreach (scandir('../assets/imgs/logos/applications', 0) as $application) {
					if (!in_array($application, $not) && ($name = getName($application))) {
			?>
			<figure>
				<img src="assets/imgs/logos/applications/<?php echo $application; ?>" alt="<?php echo $name; ?>">
				<figcaption><?php echo $name; ?></figcaption>
			</figure>
			<?php } } ?>
		</article>
	</section>
	<section id="message">
		<h3>Send me a message<span class="icon-clover"></span></h3>
		<form method="POST" name="contact">
			<div class="success_message">
				<p>You successfully sent me a message !</p>
				<p>A confirmation emailing has been sent to you.</p>
				<p>Thank you !</p>
			</div>
			<div class="error_messages"></div>
			<input type="text" name="first_name" placeholder="First name" value="">
			<input type="text" name="last_name" placeholder="Last name" value="">
			<input type="text" name="email" placeholder="Email address" value="">
			<textarea name="message" placeholder="Message (300 chars max)" maxlength="300"></textarea>
			<input type="submit" value="Send">
		</form>
	</section>
	<section id="contact">
		<h3>How to contact me<span class="icon-clover"></span></h3>
		<div>
			<article>
				<h4 class="icon-phone">Phone</h4>
				<a href="tel:+33623161906">+ 33 6 23 16 19 06</a>
			</article>
			<article>
				<h4 class="icon-mail">Email</h4>
				<a href="mailto:audreyboucher95@gmail.com">audreyboucher95@gmail.com</a>
			</article>
		</div>
		<article>
			<h4>Scan to keep in touch !</h4>
			<figure>
				<img src="assets/imgs/qrcode.png" alt="QR code">
			</figure>
			<i>To scan, download the free Unitag app on <a target="_blank" href="https://www.unitag.io/app">unitag.io/app</a></i>
		</article>
	</section>
	<footer>
		<div>
			<article>
				<!-- <a target="_blank" href="https://github.com/audreyboucher" class="icon-github"></a> -->
				<a target="_blank" href="https://bitbucket.org/audrwi/profile/repositories" class="icon-bitbucket"></a>
				<a target="_blank" href="https://www.linkedin.com/in/audrey-boucher-43247159" class="icon-linkedin"></a>
				<a target="_blank" href="http://www.viadeo.com/p/002q27q7cbj62m8" class="icon-viadeo"></a>
			</article>
			<p>&copy; Copyright 2017 <span>Audrey Boucher</span></p>
		</div>
	</footer>
	<script type="text/javascript" src="assets/js/jquery-3.1.0.min.js"></script>
	<script type="text/javascript" src="assets/js/scripts.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-40018537-1', 'audreyboucher.fr');
		ga('send', 'pageview');
	</script>
</body>
</html>
