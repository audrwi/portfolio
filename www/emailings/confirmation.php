<?php

$colors = array (
	'white'			=> '#ffffff',
	'blue'			=> '#0c2335',
	'dark_green'	=> '#26c281',
	'light_grey'	=> '#f3f5f8',
	'dark_grey'		=> '#dbe0e3'
);

$date = date("D M d, Y G:i");

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<body bgcolor="<?= $colors['blue'] ?>" style="margin:0px !important; padding:0px; -webkit-text-size-adjust:none;">
	<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="<?= $colors['blue'] ?>">
		<tr>
			<td>
				<table align="center" width="600" cellpadding="0" cellspacing="0" border="0" bgcolor="<?= $colors['white'] ?>">
					<tr height="10" bgcolor="<?= $colors['blue'] ?>">
						<td colspan="4"></td>
					</tr>
					<tr height="40" valign="center" bgcolor="<?= $colors['dark_green'] ?>">
						<td width="20"></td>
						<td width="280" align="left">
							<font face="Arial, Helvetica, sans-serif" size="2" color="<?= $colors['white'] ?>"><?= $date; ?></font>
						</td>
						<td width="280" align="right">
							<a href="http://www.<?= $_SERVER[HTTP_HOST] ?>/" color="<?= $colors['white'] ?>" title="<?= $_SERVER[HTTP_HOST] ?>" style="color:<?= $colors['white'] ?>;"><font face="Arial, Helvetica, sans-serif" size="2" color="<?= $colors['white'] ?>"><?= $_SERVER[HTTP_HOST] ?></font></a>
						</td>
						<td width="20"></td>
					</tr>
					<tr height="70">
						<td colspan="4" width="600">
							<img src="http://<?= $_SERVER[HTTP_HOST] ?>/assets/imgs/emailings_background.jpg" alt="<?= $_SERVER[HTTP_HOST] ?>" height="70" width="600" />
						</td>
					</tr>
					<tr height="30">
						<td colspan="4"></td>
					</tr>
					<tr>
						<td width="20"></td>
						<td width="560" align="left" colspan="2">
							<font face="Tahoma, Arial, Helvetica, sans-serif" size="3" color="<?= $colors['blue'] ?>">Hello <?= $_GET['first_name'] . ' ' . $_GET['last_name'] ?>,<br />You just sent me the following message on my website :</font>
						</td>
						<td width="20"></td>
					</tr>
					<tr height="30">
						<td colspan="4"></td>
					</tr>
					<tr height="15">
						<td width="20"></td>
						<td width="560" bgcolor="<?= $colors['light_grey'] ?>" style="border: 1px solid <?= $colors['dark_grey'] ?>; border-bottom: none;" align="center" colspan="2"></td>
						<td width="20"></td>
					</tr>
					<tr valign="center">
						<td width="20"></td>
						<td width="560" bgcolor="<?= $colors['light_grey'] ?>" style="border-left: 1px solid <?= $colors['dark_grey'] ?>; border-right: 1px solid <?= $colors['dark_grey'] ?>;" align="center" colspan="2">
							<table align="center" width="560" cellpadding="0" cellspacing="0" border="0" bgcolor="<?= $colors['light_grey'] ?>">
								<tr height="10">
									<td colspan="3"></td>
								</tr>
								<tr>
									<td width="20"></td>
									<td style="word-break: break-word;">
										<font face="Tahoma, Arial, Helvetica, sans-serif" size="2" color="<?= $colors['blue'] ?>"><?= nl2br($_GET['message']) ?></font>
									</td>
									<td width="20"></td>
								</tr>
								<tr height="10">
									<td colspan="3"></td>
								</tr>
							</table>
						</td>
						<td width="20"></td>
					</tr>
					<tr height="15">
						<td width="20"></td>
						<td width="560" bgcolor="<?= $colors['light_grey'] ?>" style="border: 1px solid <?= $colors['dark_grey'] ?>; border-top: none;" align="center" colspan="2"></td>
						<td width="20"></td>
					</tr>
					<tr height="30">
						<td colspan="4"></td>
					</tr>
					<tr>
						<td width="20"></td>
						<td width="560" align="left" colspan="2">
							<font face="Tahoma, Arial, Helvetica, sans-serif" size="3" color="<?= $colors['blue'] ?>">Thank you,<br />I will send you an answer as soon as possible.<br /></font>
							<font face="Tahoma, Arial, Helvetica, sans-serif" size="4" color="<?= $colors['blue'] ?>">Audrey Boucher</font>
						</td>
						<td width="20"></td>
					</tr>
					<tr height="30">
						<td colspan="4"></td>
					</tr>
					<tr height="10" bgcolor="<?= $colors['dark_green'] ?>">
						<td colspan="4"></td>
					</tr>
					<tr height="10" bgcolor="<?= $colors['blue'] ?>">
						<td colspan="4"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
