<!DOCTYPE html>
<html lang="en">
<head>
	<script type="text/javascript" src="//try.abtasty.com/d44a056c5b912dac8ecbc07ab7d71745.js"></script>
	<meta charset="utf-8">
	<title>Portfolio - Error 404</title>
	<meta name="author" content="Audrey Boucher">
	<meta name="copyright" content="&copy; Copyright 2017 | Audrey Boucher">
	<meta name="description" content="">
	<meta name="keywords" lang="en" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="robots" content="noindex, nofollow">
	<link href="/assets/css/styles.css" rel="stylesheet">
	<link rel="icon" href="/assets/imgs/favicon/favicon.ico">
	<link rel="apple-touch-icon" sizes="152x152" href="/assets/imgs/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/assets/imgs/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/assets/imgs/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/assets/imgs/favicon/manifest.json">
	<link rel="mask-icon" href="/assets/imgs/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
</head>
<body class="error">
	<section>
		<h1>Oops, this is a 404 error</h1>
		<h2>The page you're looking for doesn't exist anymore</h2>
		<a href="/">Go to my portofolio</a>
	</section>
	<footer>
		<div>
			<article>
				<!-- <a target="_blank" href="https://github.com/audreyboucher" class="icon-github"></a> -->
				<a target="_blank" href="https://bitbucket.org/audrwi/profile/repositories" class="icon-bitbucket"></a>
				<a target="_blank" href="https://www.linkedin.com/in/audrey-boucher-43247159" class="icon-linkedin"></a>
				<a target="_blank" href="http://www.viadeo.com/p/002q27q7cbj62m8" class="icon-viadeo"></a>
			</article>
			<p>&copy; Copyright 2017 <span>Audrey Boucher</span></p>
		</div>
	</footer>
</body>
</html>
